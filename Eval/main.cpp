#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"

void AffichePersonne(Personne* pPerso) {
    cout << "Personen(" << pPerso << "): " << pPerso->GetNom() << endl;
}

Personne* CreerPersonne() {
    string nom;
    cout << "Saisissez un nom :" << endl;
    cin >> nom;
    return new Personne(nom);
}

void AfficheEquipeMoto(EquipeMoto* equipe) {
    cout << "Equipe " << equipe->GetNom() << " managee par " << equipe->GetManager()->GetNom() << endl;
    Personne** pilotes = equipe->GetPilotes();
    for (int i = 0; i < 3; i++) {
        if (pilotes[i] == nullptr) {
            cout << "        -> Pas de Pilote[" << i << "]" << endl;
        }
        else {
            cout << "        -> Pilote[" << i << "]= " << pilotes[i]->GetNom() << endl;
        }
    }
}

int main()
{
    Personne pilote_1("Fabio");
    Personne* pilote_2 = CreerPersonne();

    AffichePersonne(&pilote_1);
    AffichePersonne(pilote_2);

    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");
    equipe_1->AddPilotes(0, &pilote_1);
    equipe_1->AddPilotes(1, pilote_2);

    AfficheEquipeMoto(equipe_1);

    Personne* pil_pramac_0 = CreerPersonne();
    Personne* pil_pramac_2 = CreerPersonne();

    EquipeMoto pramac = *equipe_1;
    pramac.SetNom("Pramac");
    pramac.GetManager()->SetNom("Campignoti");
    pramac.AddPilotes(0, pil_pramac_0);
    pramac.AddPilotes(2, pil_pramac_2);

    AfficheEquipeMoto(equipe_1);
    AfficheEquipeMoto(&pramac);


    delete pilote_2;
    delete equipe_1;
}

