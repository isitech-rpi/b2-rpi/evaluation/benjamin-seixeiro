#pragma once
#include <iostream>
using namespace std;

class Personne
{
private:
	string nom;
public:
	Personne(string nom);
	virtual ~Personne();

	string GetNom() { return this->nom; }
	string GetNom() const { return this->nom; }
	void SetNom(string nom) { this->nom = nom; }
};

