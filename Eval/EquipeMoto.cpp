#include "EquipeMoto.h"

EquipeMoto::EquipeMoto(string nomEquipe, string nomManager)
{
	this->nom = nomEquipe;
	this->manager = new Personne(nomManager);
	this->lesPilotes[0], this->lesPilotes[1], this->lesPilotes[2] = nullptr;

	cout << "------> Creation d'une equipe nomme " << this->nom << endl << endl;
}

EquipeMoto::~EquipeMoto()
{
	delete this->manager;
	cout << "------> Destruction d'une equipe nomme " << this->nom << endl ;
}

void EquipeMoto::AddPilotes(unsigned int rang, Personne* pilote)
{
	if (rang < 3 && rang >= 0) {
		this->lesPilotes[rang] = pilote;
	}
	else {
		cout << "Erreur sur le rang !" << endl;
	}
}
