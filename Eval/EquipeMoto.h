#pragma once
#include <iostream>
#include "Personne.h"
using namespace std;

class EquipeMoto
{
private:
	string nom;
	Personne* manager;
	Personne* lesPilotes[3];
public:
	EquipeMoto(string nomEquipe, string nomManager);
	virtual ~EquipeMoto();
	
	string GetNom() { return this->nom; }
	string GetNom() const { return this->nom; }
	void SetNom(string nom) { this->nom = nom; }


	Personne* GetManager() { return this->manager; }
	Personne* GetManager() const { return this->manager; }

	void AddPilotes(unsigned int rang, Personne* pilote);
	Personne** GetPilotes() { return lesPilotes; }

};

