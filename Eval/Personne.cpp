#include "Personne.h"

Personne::Personne(string nom)
{
	this->nom = nom;

	cout << "------> Creation d'une personne nomme " << this->nom << endl << endl;
}

Personne::~Personne()
{
	cout << "------> Destruction d'une personne nomme " << this->nom << endl;
}
